#coding: utf-8
from django import forms
from django.contrib.auth.models import User, Group
from sap.models import *
from sap.views import *
from datetime import date
from django.forms.extras.widgets import *
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget

import user
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

class RegProfessor(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = Professores
        fields = ('username', 'password')