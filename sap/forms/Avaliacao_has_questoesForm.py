#coding: utf-8
from django import forms
from django.contrib.auth.models import User, Group
from sap.models import *
from sap.views import *
from datetime import date
from django.forms.extras.widgets import *
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget

import user
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

class Avaliacao_has_questoesForm(forms.ModelForm):
    class Meta:
        model = Avaliacao_has_questoes
        fields = ('avaliacao', 'peso', 'ordem', 'questao')
        exclude = ("aluno",)
        widgets = {'questao':forms.CheckboxSelectMultiple(),}

    def __init__(self, user=None, **kwargs):
        super(Avaliacao_has_questoesForm, self).__init__(**kwargs)
        if user:
            self.fields['avaliacao'].queryset = Avaliacao.objects.filter(professor= user.id)
            self.fields['questao'].queryset = Questoes.objects.filter(professor= user.id)
