#coding: utf-8
from django import forms
from django.contrib.auth.models import User, Group
from sap.models import *
from sap.views import *
from datetime import date
from django.forms.extras.widgets import *
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget

import user
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

class avaliacaoForm(forms.ModelForm):
    professor = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    questao = forms.CheckboxSelectMultiple
    nome = forms.CharField(max_length=128, required= True)
    data_hora_inicio = forms.DateTimeField(required= True)
    data_hora_fim = forms.DateTimeField(required= True)
    nome = forms.CharField(max_length=128, required= True)
    class Meta:
        model = Avaliacao
        fields = ('nome','data_hora_inicio','data_hora_fim','senha','SafeExamBrowser','Mostrar_Respostas','imagem')
