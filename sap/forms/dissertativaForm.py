#coding: utf-8
from django import forms
from django.contrib.auth.models import User, Group
from sap.models import Questoes
from sap.views import *
from datetime import date
from django.forms.extras.widgets import *
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget

import user
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

class dissertativaForm(forms.ModelForm):
    professor = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    enunciado = forms.CharField(widget= forms.Textarea, required= True)
    resposta_correta = forms.CharField(widget= forms.Textarea, required= True)

    class Meta:
        model = Questoes
        fields = ("enunciado", "resposta_correta", "disciplina", "imagem")
        widgets = {'disciplina':forms.CheckboxSelectMultiple(),}
