#coding: utf-8
from django.shortcuts import render, redirect

from dissertativaForm import *
from Avaliacao_has_questoesForm import *
from avaliacaoForm import *
from correcaoForm import *
from diciplinaForm import *
from disciplina_has_alunoForm import *
from multipla_escolhaForm import *
from reaplicarForm import *
from RegAluno import *
from RegProfessor import *
from respostasForm import *
from TypeUserForm import *
from  verdadeiro_falsoForm import *