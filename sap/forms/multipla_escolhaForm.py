#coding: utf-8
from django import forms
from django.contrib.auth.models import User, Group
from sap.models import *
from sap.views import *
from datetime import date
from django.forms.extras.widgets import *
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget

import user
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

class multipla_escolhaForm(forms.ModelForm):
    professor = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    resposta_correta = forms.CharField(widget=forms.HiddenInput())
    a = forms.CharField(widget= forms.Textarea, required= True)
    b = forms.CharField(widget= forms.Textarea, required= True)
    c = forms.CharField(widget= forms.Textarea, required= True)
    d = forms.CharField(widget= forms.Textarea, required= True)
    e = forms.CharField(widget= forms.Textarea, required= True)


    class Meta:
        model = Questoes
        fields = ("enunciado", "a", "b", "c", "d", "e", "disciplina", "imagem")
        widgets = {'disciplina':forms.CheckboxSelectMultiple(),}
