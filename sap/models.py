#coding: utf-8

import datetime
from django.db import models
from django.contrib.auth.models import User, Group
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

class Alunos(User):
    class Meta:
        verbose_name_plural = 'Alunos'
    def __unicode__(self):
        return self.username
class Disciplina(models.Model):
    nome = models.CharField(max_length=250)
    disciplina = models.ManyToManyField(Alunos,'Disciplina_has_alunos')
    def __unicode__(self):
        return self.nome
class Disciplina_has_aluno(models.Model):
    disciplina = models.ForeignKey('Disciplina')
    aluno = models.ForeignKey('Alunos')
class Professores(User):
    class Meta:
        verbose_name_plural = 'Professores'
class Tipo(models.Model):
    tipo = models.CharField(max_length=250)
    def __unicode__(self):
        return self.tipo

class Questoes(models.Model):
    tipo = models.ForeignKey('Tipo')
    professor = models.ForeignKey('Professores')
    disciplina = models.ManyToManyField('Disciplina')
    enunciado = models.TextField(max_length=5000, unique=False)
    a = models.TextField(max_length=5000, unique=False, blank=True)
    b = models.TextField(max_length=5000, unique=False, blank=True)
    c = models.TextField(max_length=5000, unique=False, blank=True)
    d = models.TextField(max_length=5000, unique=False, blank=True)
    e = models.TextField(max_length=5000, unique=False, blank=True)
    resposta_correta = models.TextField(max_length=5000, unique=False)
    imagem = models.ImageField(upload_to='questao_imagem',blank=True)

    def __unicode__(self):
        return self.enunciado

class Avaliacao(models.Model):
    professor = models.ForeignKey('Professores')
    nome = models.CharField(max_length=250)
    data_hora_inicio = models.DateTimeField(auto_now=False)
    data_hora_fim = models.DateTimeField(auto_now=False)
    senha = models.CharField(max_length=20)
    questao = models.ManyToManyField(Questoes,through='Avaliacao_has_questoes')
    disciplina = models.ForeignKey('Disciplina')
    SafeExamBrowser = models.BooleanField(default=False)
    Mostrar_Respostas = models.BooleanField(default=False)
    imagem = models.ImageField(upload_to='avaliacao_imagem',blank=True)



    def __unicode__(self):
        return self.nome
class Avaliacao_has_questoes(models.Model):
    avaliacao = models.ForeignKey('Avaliacao')
    questao = models.ForeignKey('Questoes')
    peso = models.FloatField(default=1)
    ordem = models.IntegerField(default=0)
    aluno = models.ManyToManyField(Alunos,'Respostas')

    def __unicode__(self):
        return unicode(self.questao)
class Respostas(models.Model):
    aluno = models.ForeignKey('Alunos')
    avaliacao_has_questoes = models.ForeignKey('Avaliacao_has_questoes')
    resposta_correta = models.TextField(max_length=5000, unique=False)
    pontuacao = models.FloatField(default=0)

    def __unicode__(self):
        return self.resposta_correta

class Aluno_has_avaliacao(models.Model):
    aluno = models.ForeignKey('Alunos')
    avaliacao = models.ForeignKey('Avaliacao')
    data_hora = models.DateTimeField(auto_now=True)
    nota_final = models.FloatField(default=0)

