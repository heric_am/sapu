#coding: utf-8
from django.conf.urls import patterns, url
from sap import views
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
urlpatterns = patterns('',
                       url(r'^$', views.user_login, name='index'),
                       #Admin
                       url(r'^index_professor',views.index_professor, name='index_professor'),
                       url(r'^index_aluno',views.index_aluno, name='index_aluno'),
                       url(r'^admin',views.admin, name='admin'),
                       url(r'^register_professor',views.register_professor, name='register_professor'),
                       url(r'^register_aluno',views.register_aluno, name='register_aluno'),
                       url(r'^adicionar_diciplina',views.adicionar_diciplina, name='adicionar_diciplina'),
                       url(r'^adicionar_disciplina_aluno',views.adicionar_disciplina_aluno, name='adicionar_disciplina_aluno'),
                       url(r'^relatorio_professor',views.relatorio_professor, name='relatorio_professor'),
                       url(r'^login',views.user_login, name='login'),
                       url(r'^logout',views.user_logout, name='logout'),
                       #Professor
                       url(r'^criar_dissertativa',views.criar_dissertativa, name='criar_dissertativa'),
                       url(r'^criar_multipla_escolha',views.criar_multipla_escolha, name='criar_multipla_escolha'),
                       url(r'^criar_verdadeiro_falso',views.criar_verdadeiro_falso, name='criar_verdadeiro_falso'),
                       url(r'^editar_questao',views.editar_questao, name='editar_questao'),
                       url(r'^criar_avaliacao',views.criar_avaliacao, name='criar_avaliacao'),
                       url(r'^adicionar_remover_pergunta/(?P<avaliacao>[\w\-]+)',views.adicionar_remover_pergunta, name='adicionar_remover_pergunta'),
                       url(r'^alterar_dados_avaliacao/(?P<avaliacao>[\w\-]+)',views.alterar_dados_avaliacao, name='alterar_dados_avaliacao'),
                       url(r'^editar_avaliacao',views.editar_avaliacao, name='editar_avaliacao'),
                       url(r'^corrigir_avaliacao',views.corrigir_avaliacao, name='corrigir_avaliacao'),
                       url(r'^vizualizar_avaliacao',views.vizualizar_avaliacao, name='vizualizar_avaliacao'),
                       url(r'^visualizar_gabarito', views.visualizar_gabarito, name='visualizar_gabarito'),
                       url(r'^correcao_questao/(?P<avaliacao>[\w\-]+)',views.correcao_questao, name='correcao_questao'),
                       url(r'^correcao_aluno/(?P<avaliacao>[\w\-]+)',views.correcao_aluno, name='correcao_aluno'),
                       url(r'^relatorio_professor',views.relatorio_professor, name='relatorio_professor'),
                       url(r'^reaplicar_avaliacao',views.reaplicar_avaliacao, name='reaplicar_avaliacao'),
                       url(r'^excluir_avaliacao',views.excluir_avaliacao, name='excluir_avaliacao'),
                       url(r'^excluir_questao',views.excluir_questao, name='excluir_questao'),
                       url(r'^serverTime',views.serverTime, name='serverTime'),


                       #Aluno
                       url(r'^realizar_avaliacao',views.realizar_avaliacao, name='realizar_avaliaçao'),
                       url(r'^notas',views.notas, name='notas'),
                       url(r'^avaliacao',views.avaliacao, name='avaliacao'),
                       url(r'^sair_safeexam',views.sair_safeexam, name='sair_safeexam'),

                       )