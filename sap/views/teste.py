import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
def teste(request):
    context_dict = {}

    if request.method !=  "POST":

        data_atual = datetime.datetime.now()

        context_dict['data_atual'] = data_atual.strftime('%Y/%m/%d %H:%M:%S')
    else:
        pass


    template ='sap/teste.html'
    return render(request, template , context_dict)