#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def excluir_questao(request):
    context_dict ={}
    if request.method != "POST":
        context_dict['disciplina'] = Disciplina.objects.all()
    else:
        if 'disciplina' in request.POST:
            questoes_disciplina_professor = Questoes.objects.filter(disciplina= request.POST['disciplina']).filter(professor= request.user)
            if questoes_disciplina_professor:
                context_dict['questoes_disciplina_professor'] = questoes_disciplina_professor
            else:
                context_dict['falha'] = "Não existem questões vinculadas a essa disciplina."

        if 'questao' in request.POST:
            try:
                questao = request.POST['questao']
                existe = Avaliacao_has_questoes.objects.filter(questao= Questoes.objects.get(id= questao)).exists()
                if existe == True:
                     context_dict['falha'] = "Você não pode excluir esta questão, pois ela esta vinculada a uma avaliação."
                else:
                    Questoes.objects.get(id= questao).delete()
                    context_dict['sucess'] = "Questão excluida com sucesso."
            except:
                     context_dict['falha'] = "Você não pode excluir esta questão."

    template = 'sap/excluir_questao.html'
    return render(request,template,context_dict)