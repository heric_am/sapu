#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def excluir_avaliacao(request):
    context_dict ={}
    if request.method != "POST":
        avaliacao_respondida = Aluno_has_avaliacao.objects.values('avaliacao')
        avaliacao = Avaliacao.objects.filter(professor= request.user.id).order_by('-data_hora_fim').exclude(id__in= avaliacao_respondida)
        if avaliacao:
            context_dict['avaliacao'] = avaliacao
        else:
            context_dict['falha'] = "Não existem avaliações."

        context_dict['avaliacao'] = avaliacao
    else:
        if 'avaliacao' in request.POST:
            try:
                avaliacao = request.POST['avaliacao']
                existe = Aluno_has_avaliacao.objects.filter(avaliacao= avaliacao).exists()
                if existe == True:
                    context_dict['falha'] = "Você não pode excluir esta avaliação, pois ela já foi respondida."
                else:
                    Avaliacao.objects.get(id= avaliacao).delete()
                    context_dict['sucess'] = "Avaliação removida com sucesso."
            except:
                context_dict['falha'] = "Você não pode excluir esta avaliação."
        else:
            context_dict['falha'] = "Você tem deve selecionar uma avaliação."

    template = 'sap/excluir_avaliacao.html'
    return render(request,template,context_dict)
