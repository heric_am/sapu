#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_aluno, login_url='login.html')
def realizar_avaliacao(request):
    context_dict ={}
    if request.method !=  "POST":
        disciplina_aluno = Disciplina_has_aluno.objects.filter(aluno= request.user.id).values_list('disciplina', flat= True)
        avaliacao_aluno = Aluno_has_avaliacao.objects.filter(aluno= request.user.id).values_list('avaliacao', flat= True)
        avaliacao = Avaliacao.objects.filter(disciplina= disciplina_aluno, data_hora_fim__gt= datetime.datetime.now()).exclude(id__in= avaliacao_aluno)
        if avaliacao:
            context_dict['avaliacao'] = avaliacao
        else:
            context_dict['falha'] = "Não existem avaliações disponiveis atualmente."

    else:
        avaliacaoform = request.POST
        try:
            id_avaliacao = avaliacaoform['avaliacao']
            prova_feita = Aluno_has_avaliacao.objects.filter(avaliacao= id_avaliacao, aluno= request.user.id).exists()
            if prova_feita == True:
                context_dict['falha'] = 'Você já respondeu a esta avaliação.'
            questoes_inseridas = Avaliacao_has_questoes.objects.filter(avaliacao= id_avaliacao).exists()
            if questoes_inseridas == False:
                context_dict['falha'] = "Nenhuma questão foi inserida nesta avaliação."

            senha = request.POST['senha']
            data = Avaliacao.objects.get(id= id_avaliacao)
            context_dict['avaliacao'] = id_avaliacao
            inicio = data.data_hora_inicio.replace(tzinfo= None)
            fim = data.data_hora_fim.replace(tzinfo= None)
            if datetime.datetime.now() >= inicio and datetime.datetime.now() <= fim:
                if senha == data.senha:
                    return render(request,'sap/avaliacao.html',context_dict)
                else:
                    context_dict['falha'] = 'Senha incorreta.'
            else:
                context_dict['falha'] = 'Esta avaliação ainda não esta disponível.'
        except:
            context_dict['falha'] = 'Você deve selecionar uma avaliação.'
    template = 'sap/realizar_avaliacao.html'
    return render(request,template,context_dict)
