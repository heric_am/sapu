#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def adicionar_remover_pergunta(request, avaliacao):
    context_dict = {}
    context_dict['avaliacao'] = avaliacao
    if request.method != "POST":
        try:
            Avaliacao.objects.get(id= avaliacao, professor= request.user)
            existe = Aluno_has_avaliacao.objects.filter(avaliacao= avaliacao).exists()
            a = Avaliacao.objects.get(id= avaliacao)
            if existe == True or a.data_hora_inicio < datetime.datetime.now() and a.data_hora_fim > datetime.datetime.now():
                context_dict['vazio'] = "Você não pode editar esta avaliação."
            else:
                questoes_inclusas = Avaliacao_has_questoes.objects.filter(avaliacao= Avaliacao.objects.get(id= avaliacao)).order_by('ordem')
                questoes_inclusas2 = Questoes.objects.filter(avaliacao= avaliacao)
                avaliacaoob = Avaliacao.objects.get(id= avaliacao)
                questoes_nao_inclusas = Questoes.objects.filter(professor = request.user, disciplina= avaliacaoob.disciplina).exclude(id__in= questoes_inclusas2).order_by()
                context_dict['questoes_nao_inclusas'] = questoes_nao_inclusas
                context_dict['questoes_inclusas'] = questoes_inclusas

        except:
            context_dict['vazio'] = "Você não pode editar esta avaliação."
    else:
        excluir = request.POST.getlist('excluir')
        adicionar = request.POST.getlist('adicionar')
        peso = filter(None,request.POST.getlist('peso'))
        ordem = filter(None,request.POST.getlist('ordem'))
        print excluir
        if excluir or adicionar:
            if excluir:
                for x in range(0,len(excluir)):
                    try:
                        Avaliacao_has_questoes.objects.get(avaliacao= (Avaliacao.objects.get(id= avaliacao)), questao= Questoes.objects.get(id= excluir[x])).delete()
                    except:
                        return redirect('/sap/index_professor.html')
                context_dict['excluidas'] = Questoes.objects.filter(id__in= excluir)
            if adicionar:
                for x in range(0,len(adicionar)):
                    Avaliacao_has_questoes.objects.get_or_create(avaliacao= (Avaliacao.objects.get(id= avaliacao)), questao= Questoes.objects.get(id= adicionar[x]),peso= peso[x], ordem= ordem[x])
                context_dict['adicionadas'] = Questoes.objects.filter(id__in= adicionar)
                ordem = Avaliacao_has_questoes.objects.filter(avaliacao= (Avaliacao.objects.get(id= avaliacao))).values_list('ordem', flat= True)
                questoes = Avaliacao_has_questoes.objects.filter(avaliacao= (Avaliacao.objects.get(id= avaliacao))).values_list('questao', flat= True)
                #Criar função \/
                conv = []
                for item in ordem:
                    conv.append(int(item))
                ordem = conv
                na= len(ordem)
                nova_lista = list(set(ordem))
                nnova_list = len(nova_lista)
                print ordem, na, nova_lista, nnova_list
                if na != nnova_list:
                    for v in range (len(nova_lista), (na)):
                        nova_lista.append(int(max(nova_lista)) + 1)
                    ordem = nova_lista
                    print ordem
                    questoes = sorted(questoes)
                    print questoes
                    #Criar função /\
                    for v in range (0,int(len(ordem))):
                        Avaliacao_has_questoes.objects.filter(avaliacao= Avaliacao.objects.get(id= avaliacao), questao= questoes[v]).update(ordem= ordem[v])

        else:
             context_dict['sem_selecao'] = "Você deve selecionar ao menos uma questão"

    template ='sap/adicionar_remover_pergunta.html'
    return render(request, template , context_dict)
