#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def adicionar_diciplina(request):
    context_dict = {}
    if request.method != "POST":
        diciplina_form = diciplinaForm()
    else:
        diciplina_form = diciplinaForm(data=request.POST)
        if diciplina_form.is_valid():
            nome = diciplina_form.save()
            nome.save()

    context_dict['diciplina_form'] = diciplina_form
    template ='sap/adicionar_diciplina.html'
    return render(request, template , context_dict)