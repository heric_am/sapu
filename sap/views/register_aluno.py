#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def register_aluno(request):
    context_dict = {}
    registered = False
    if request.method != "POST":
        user_form = RegAluno()
    else:
        user_form = RegAluno(data=request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            Group.objects.get_or_create(name='Alunos')
            add_grupo = Group.objects.get(name='Alunos')
            add_grupo.user_set.add(user)
#
    context_dict['registered'] = registered
    context_dict['user_form'] = user_form
    return render(request, 'sap/register_aluno.html', context_dict)