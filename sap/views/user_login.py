#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
def user_login(request):
    context_dict = {}
    if request.method != "POST":
        print request.META['HTTP_USER_AGENT']
        if request.META['HTTP_USER_AGENT'] == 'IFC SEB 2.1.1':
            context_dict['navegador'] =  'seb'
        template ='sap/login.html'
        return render(request, template , context_dict)
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                try:
                    user_professor = Group.objects.get(name="Professores").user_set.all()
                    if user in user_professor:
                        login(request, user)
                        return HttpResponseRedirect('/sap/index_professor.html')
                except Group.objects.get(name="Professores").user_set.all().DoesNotExist():
                    pass

                try:
                    user_aluno = Group.objects.get(name="Alunos").user_set.all()
                    if user in user_aluno:
                        login(request, user)
                        return HttpResponseRedirect('/sap/index_aluno.html', context_dict)
                except Group.objects.get(name="Alunos").user_set.all().DoesNotExist():
                    pass
            else:
                return HttpResponse("Sua conta esta desabilitada")
        else:
            context_dict['falha'] = 'Dados inválidos'
            template ='sap/login.html'
            return render(request, template , context_dict)