#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from django.db.models import Sum
import datetime
from check_professor import *
from check_aluno import *
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def adicionar_disciplina_aluno(request):
    context_dict = {}
    if request.method != "POST":
        form = disciplina_has_alunoForm()
    else:
        form = disciplina_has_alunoForm(data=request.POST)
        if form.is_valid():
            nome = form.save()
            nome.save()
    context_dict['form'] = form
    template ='sap/adicionar_disciplina_aluno.html'
    return render(request, template , context_dict)