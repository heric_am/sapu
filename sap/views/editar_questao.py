#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from PIL import Image
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def editar_questao(request):
    context_dict ={}
    if request.method != "POST":
        context_dict['disciplina'] = Disciplina.objects.all()
    else:
        if 'disciplina' in request.POST:
            questoes = Questoes.objects.filter(disciplina= request.POST['disciplina'], professor= request.user.id)
            if questoes:
                context_dict['questoes'] = questoes
            else:
                context_dict['falha'] = "Não existem questões vinculadas a esta matéria"

        if 'questao' in request.POST:
            questao = Questoes.objects.get(id = request.POST['questao'])
            existe = Avaliacao_has_questoes.objects.filter(questao= questao).exists()
            if existe == False:
                context_dict['diciplinas_questao']  = Disciplina.objects.filter(questoes= questao.id)
                context_dict['disciplinas_n_questao'] = Disciplina.objects.all().exclude(questoes= questao.id)
                if str(questao.tipo) == "Dissertativa":
                    context_dict['dissertativa'] = questao
                else:
                    context_dict['multipla_escolha'] = questao
            else:
                context_dict['falha'] = "Você não pode editar esta questão. Ela está vinculada a uma avaliação."

        if 'Dissertativa' in request.POST:
            f = dissertativaForm(data=request.POST)
            if f.is_valid:

                disciplinas_escolhidas = request.POST.getlist('disciplina')
                if disciplinas_escolhidas:
                    qd = f.save(commit=False)
                    questaon = request.POST['questaon']
                    Questoes.disciplina.through.objects.filter(questoes= questaon).delete()
                    #não foi possivel usar o update para atualizar as imagens, foi necessario deletar e recriar a questão.
                    if 'imagem' in request.FILES:
                        imagem = request.FILES['imagem']
                        try:
                            im= Image.open(imagem)
                            qd.id = questaon
                            qd.professor = Professores.objects.get(id= request.user.id)
                            qd.tipo = Tipo.objects.get(id= 1)
                            qd.imagem = imagem
                            qd.save()
                        except:
                            context_dict['falha'] = 'Formato de imagem não suportado.'
                            template = 'sap/editar_questao.html'
                            return render(request,template,context_dict)
                    else:
                        if 'deletar' in request.POST:
                            Questoes.objects.filter(id= questaon).update(imagem= '')
                        else:
                            Questoes.objects.filter(id= questaon).update(enunciado= request.POST['enunciado'], resposta_correta= request.POST['resposta_correta'])

                    for x in range(0,len(disciplinas_escolhidas)):
                         Questoes.disciplina.through.objects.get_or_create(questoes= Questoes.objects.get(id= questaon), disciplina= Disciplina.objects.get(id= disciplinas_escolhidas[x]))
                    context_dict['sucess'] = 'Alteração realizada com sucesso.'
                else:
                    context_dict['falha'] = "Você deve selecionar ao menos uma disciplina."
            else:
                context_dict['falha'] = "O formulario não foi validado"

        if 'Multipla Escolha' in request.POST:
            f = multipla_escolhaForm(data=request.POST)
            if f.is_valid:
                disciplinas_escolhidas = request.POST.getlist('disciplina')
                if disciplinas_escolhidas:
                    print request.POST
                    qme = f.save(commit=False)
                    questaon = request.POST['questaon']
                    Questoes.disciplina.through.objects.filter(questoes= questaon).delete()
                    if 'imagem' in request.FILES:
                        imagem = request.FILES['imagem']
                        try:
                            im= Image.open(imagem)
                            Questoes.objects.get(id= questaon).delete()
                            qme.id = questaon
                            qme.professor = Professores.objects.get(id= request.user.id)
                            qme.tipo = Tipo.objects.get(id= 2)
                            qme.imagem = imagem
                            qme.save()
                        except:
                            context_dict['falha'] = 'Formato de imagem não suportado.'
                            template = 'sap/editar_questao.html'
                            return render(request,template,context_dict)
                    else:
                        if 'deletar' in request.POST:
                            Questoes.objects.filter(id= questaon).update(imagem= '')
                        else:
                            Questoes.objects.filter(id= questaon).update(enunciado= request.POST['enunciado'],
                                                                        a= request.POST['a'], b= request.POST['b'], c= request.POST['c'],
                                                                        d= request.POST['d'],e= request.POST['e'],resposta_correta= request.POST['resposta_correta'])
                    #não foi possivel usar o update para atualizar as imagens, foi necessario deletar e recriar a questão.

                    for x in range(0,len(disciplinas_escolhidas)):
                        Questoes.disciplina.through.objects.get_or_create(questoes= Questoes.objects.get(id= questaon), disciplina= Disciplina.objects.get(id= disciplinas_escolhidas[x]))
                    context_dict['sucess'] = 'Alteração realizada com sucesso.'
                else:
                    context_dict['falha'] = "Você deve selecionar ao menos uma disciplina."
            else:
                context_dict['falha'] = "O formulario não foi validado"


    template = 'sap/editar_questao.html'
    return render(request,template,context_dict)
