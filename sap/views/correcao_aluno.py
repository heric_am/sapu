#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def correcao_aluno(request, avaliacao):
    context_dict ={}
    if request.method != 'POST':
        id_avaliacao = avaliacao
        alunos = Aluno_has_avaliacao.objects.filter(avaliacao= id_avaliacao)
        questoes = Questoes.objects.filter(avaliacao= avaliacao, tipo= 1, professor= request.user.id)
        print questoes
        if questoes:
            context_dict['questoes'] = questoes
        else:
            context_dict['falha'] = "Está avaliação não possui questões dissertativas."
        context_dict['avaliacao']= id_avaliacao
        context_dict['alunos'] = alunos
    else:
        id_avaliacao = avaliacao
        aluno =  request.POST['aluno_id']
        if 'resposta_id' in request.POST:
            print request.POST
            resposta_id = request.POST.getlist('resposta_id')
            pontuacao = request.POST.getlist('pontuacao')
            aluno = request.POST['aluno_id']
            for x in range (0,int(len(resposta_id))):
                if pontuacao[x] != '':
                    Respostas.objects.filter(id= resposta_id[x]).update(pontuacao= pontuacao[x])
                else:
                    pass
            av_qu = Avaliacao_has_questoes.objects.filter(avaliacao= avaliacao).values_list('id', flat= True)
            pontos = Respostas.objects.filter(avaliacao_has_questoes= av_qu, aluno= aluno).aggregate(Sum('pontuacao'))
            pontos = pontos['pontuacao__sum']
            avaliacao_questao_peso = Avaliacao_has_questoes.objects.filter(avaliacao= id_avaliacao).values_list('peso', flat= True)
            nota_final = 10/(sum(avaliacao_questao_peso)/pontos)
            Aluno_has_avaliacao.objects.filter(avaliacao= avaliacao, aluno= aluno).update(nota_final= round(nota_final,2))
            context_dict['avaliacao'] = avaliacao
            alunos = Aluno_has_avaliacao.objects.filter(avaliacao= id_avaliacao)
            context_dict['alunos'] = alunos
            context_dict['questoes'] = Questoes.objects.filter(avaliacao= avaliacao, tipo= 1)
            context_dict['sucesso'] = "Questões corrigidas com sucesso."
        else:
            #django não permite usar a variavel do for de um objeto para contar outro. esta foi a unica forma encontrada até o momento para fazer isso
            avaliacao_questao = Avaliacao_has_questoes.objects.filter(avaliacao= id_avaliacao).order_by('ordem')
            respostas_aluno = Respostas.objects.filter(avaliacao_has_questoes= avaliacao_questao, aluno= aluno).values_list('resposta_correta', flat= True).order_by('avaliacao_has_questoes__ordem')
            respostas_aluno_id = Respostas.objects.filter(avaliacao_has_questoes= avaliacao_questao, aluno= aluno).values_list('id', flat= True).order_by('avaliacao_has_questoes__ordem')
            respostas_aluno_pontuacao = Respostas.objects.filter(avaliacao_has_questoes= avaliacao_questao, aluno= aluno).values_list('pontuacao', flat= True).order_by('avaliacao_has_questoes__ordem')
            context_dict['avaliacao'] = id_avaliacao
            context_dict['aluno_id'] = aluno
            context_dict['avaliacao_questao'] = avaliacao_questao
            context_dict['respostas_aluno'] = respostas_aluno
            context_dict['respostas_aluno_id'] = respostas_aluno_id
            context_dict['respostas_aluno_pontuacao'] = respostas_aluno_pontuacao
            print avaliacao_questao
            print respostas_aluno


    template = 'sap/correcao_aluno.html'
    return render(request,template,context_dict)