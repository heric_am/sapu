#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from PIL import Image
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def alterar_dados_avaliacao(request,avaliacao):
    context_dict = {}
    if request.method != "POST":
        try:
            Avaliacao.objects.get(id= avaliacao, professor= request.user)
            existe = Aluno_has_avaliacao.objects.filter(avaliacao= avaliacao).exists()
            a = Avaliacao.objects.get(id= avaliacao)
            if existe == True or a.data_hora_inicio < datetime.datetime.now() and a.data_hora_fim > datetime.datetime.now():
                context_dict['falha'] = "Você não pode alterar esta avaliação"
            else:
                context_dict['form'] = reaplicarForm()
                context_dict['avaliacao'] = Avaliacao.objects.get(id=avaliacao)
        except:
            context_dict['falha'] = "Você não pode alterar esta avaliação"
    else:
        form = reaplicarForm(data= request.POST)
        if 'deletar' in request.POST:
            Avaliacao.objects.filter(id= avaliacao).update(imagem= '')
            context_dict['sucesso'] = "Alteração realizada com sucesso"
            template = 'sap/alterar_dados_avaliacao.html'
            return render(request,template,context_dict)
        if form.is_valid():
            f = form.save(commit=False)
            if f.data_hora_inicio < datetime.datetime.now():
                context_dict['data_invalida'] = "Você não pode escolher uma data anterior a atual."
                template ='sap/alterar_dados_avaliacao.html'
                return render(request, template , context_dict)
            if f.data_hora_inicio < f.data_hora_fim:
                f.professor = Professores.objects.get(id=request.user.id)
                if 'imagem' in request.FILES:
                    imagem = request.FILES['imagem']
                    try:
                        im= Image.open(imagem)
                        f.id = avaliacao
                        f.imagem = imagem
                        f.save()
                    except:
                        context_dict['falha'] = 'Formato de imagem não suportado.'
                        template = 'sap/alterar_dados_avaliacao.html'
                        return render(request,template,context_dict)
                else:
                     Avaliacao.objects.filter(id= avaliacao).update(nome= f.nome, data_hora_inicio= f.data_hora_inicio, data_hora_fim= f.data_hora_fim, senha= f.senha, disciplina= f.disciplina, SafeExamBrowser= f.SafeExamBrowser, Mostrar_Respostas= f.Mostrar_Respostas)

                context_dict['sucesso'] = "Alteração realizada com sucesso"
            else:
                context_dict['data_invalida'] = "A data é invalida."
        else:
            context_dict['vazio'] = "Os campos Nome, Data hora inicio, Data hora fim, Senha e Disciplina devem ser preenchidos."
    template = 'sap/alterar_dados_avaliacao.html'
    return render(request,template,context_dict)
