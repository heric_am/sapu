#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def vizualizar_avaliacao(request):
    context_dict = {}
    if request.method != "POST":
        avaliacao = Avaliacao.objects.filter(professor= request.user.id)
        if avaliacao:
            context_dict['avaliacao'] = avaliacao
        else:
            context_dict['falha1'] = "Não existem avaliações"
    else:
        if 'avaliacao' in request.POST:
            avaliacao = Avaliacao.objects.get(id=request.POST['avaliacao'])
            questoes = Questoes.objects.filter(avaliacao= avaliacao)
            context_dict['avaliacao'] = avaliacao
            context_dict['questoes'] = questoes

    template ='sap/vizualizar_avaliacao.html'
    return render(request, template , context_dict)
