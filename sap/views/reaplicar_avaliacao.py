#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from PIL import Image
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def reaplicar_avaliacao(request):
    context_dict ={}
    if request.method != "POST":
        avaliacao = Avaliacao.objects.filter(professor= request.user.id)
        if avaliacao:
            context_dict['avaliacao'] = avaliacao
            context_dict['form'] = reaplicarForm
            context_dict['disciplina'] = Disciplina.objects.all()
        else:
            context_dict['falha1'] = "Não existem avaliações"
    else:
        if 'avaliacao' in request.POST:
            form = reaplicarForm(data= request.POST)
            try:
                if form.is_valid:
                    f = form.save(commit=False)
                    if f.data_hora_inicio < datetime.datetime.now():
                        context_dict['falha'] = "Você não pode escolher uma data anterior a atual."
                        template ='sap/reaplicar_avaliacao.html'
                        return render(request, template , context_dict)
                    if f.data_hora_inicio < f.data_hora_fim:
                        avaliacao = Avaliacao.objects.get(id= request.POST['avaliacao'])
                        copia_avaliacao = Avaliacao.objects.get(id= avaliacao.id)
                        copia_avaliacao.pk = None
                        if 'imagem' in request.FILES:
                            imagem = request.FILES['imagem']
                            try:
                                im= Image.open(imagem)
                                copia_avaliacao.imagem = imagem
                                copia_avaliacao.save()
                            except:
                                context_dict['falha'] = 'Formato de imagem não suportado.'
                                template = 'sap/reaplicar_avaliacao.html'
                                return render(request,template,context_dict)
                        copia_avaliacao.save()
                        questoes = Avaliacao_has_questoes.objects.filter(avaliacao= avaliacao.id).values_list('questao', flat=True)
                        peso = Avaliacao_has_questoes.objects.filter(avaliacao= avaliacao.id).values_list('peso',flat= True)
                        ordem = Avaliacao_has_questoes.objects.filter(avaliacao= avaliacao.id).values_list('ordem',flat= True)
                        id_copia = Avaliacao.objects.filter(professor= request.user).last()
                        for v in range(0,questoes.count()):
                            Avaliacao_has_questoes.objects.get_or_create(avaliacao= id_copia, questao= Questoes.objects.get(id= questoes[v]),peso= peso[v], ordem= ordem[v])
                        Avaliacao.objects.filter(id= id_copia.id).update(nome= f.nome, data_hora_inicio= f.data_hora_inicio, data_hora_fim= f.data_hora_fim, senha= f.senha, disciplina= f.disciplina, SafeExamBrowser= f.SafeExamBrowser, Mostrar_Respostas= f.Mostrar_Respostas)
                        context_dict['sucess'] = "Alteração realizada com sucesso."
                    else:
                        context_dict['falha'] = "Data invalida."
            except:
                 context_dict['forminvalido'] = "Você deve preencer todos os campos."
    template = 'sap/reaplicar_avaliacao.html'
    return render(request,template,context_dict)
