#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum

import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_aluno, login_url='login.html')
@login_required()
@user_passes_test(check_aluno, login_url='login.html')
def avaliacao(request):
    context_dict = {}
    if request.method !=  "POST":
        return redirect('/sap/realizar_avaliacao.html')
    else:
        id_avaliacao = request.POST['avaliacao']
        if Avaliacao.objects.get(id= id_avaliacao).SafeExamBrowser == True and request.META['HTTP_USER_AGENT'] != 'IFC SEB 2.1.1': #verifica o navegador
                context_dict['falha'] = "Você deve utilizar o Safe Exam Browser para responder está avaliação juntamente com o arquivo de configuração do IFC."
        else:
            context_dict['nome_avaliacao'] = Avaliacao.objects.get(id= id_avaliacao)
            data_fim = Avaliacao.objects.get(id= id_avaliacao).data_hora_fim
            data_atual = datetime.datetime.now()
            tempo_restante = data_fim - data_atual
            context_dict['data_atual'] = data_atual.strftime('%Y/%m/%d %H:%M:%S')
            context_dict['data_fim'] = data_fim.strftime('%Y/%m/%d %H:%M:%S')
            context_dict['tempo_restante'] = tempo_restante

            context_dict['numero_de_questoes'] = Avaliacao_has_questoes.objects.filter(avaliacao= id_avaliacao).count()
            existe = Aluno_has_avaliacao.objects.filter(avaliacao= id_avaliacao, aluno= request.user.id).exists() #verifica se aluno ja submeteu a avaliação
            context_dict['avaliacao'] = id_avaliacao
            if existe != True:
                questoes_avaliacao = Avaliacao_has_questoes.objects.filter(avaliacao= id_avaliacao).order_by('ordem')
                for x in range(0,len(questoes_avaliacao)):
                    existe = Respostas.objects.filter(avaliacao_has_questoes= questoes_avaliacao[x], aluno= request.user.id).exists()
                    if existe == False: #verifica se a questão ja foi respondida
                        Respostas.objects.get_or_create(avaliacao_has_questoes= Avaliacao_has_questoes.objects.get(id= questoes_avaliacao[x].id),
                                                        aluno= Alunos.objects.get(id=request.user.id))

                context_dict['respostas'] = Respostas.objects.filter(avaliacao_has_questoes= questoes_avaliacao, aluno= request.user.id, ).\
                    values_list('resposta_correta', flat= True).order_by('avaliacao_has_questoes__ordem')
                context_dict['questoes'] = questoes_avaliacao
                if 'resposta_dissertativa' in request.POST:
                    Respostas.objects.filter(avaliacao_has_questoes= request.POST['questao_avaliacao'], aluno= Alunos.objects.get(id= request.user.id)).update(resposta_correta= request.POST['resposta'])

                if 'resposta_multipla_escolha' in request.POST:
                    id_q = request.POST['questao_avaliacao']
                    qa = Avaliacao_has_questoes.objects.filter(id= id_q).values_list('questao',flat= True)
                    if 'resposta' in request.POST:
                        ra = request.POST['resposta']
                        rc = Questoes.objects.filter(id= qa).values_list('resposta_correta', flat= True)
                        peso_qa = Avaliacao_has_questoes.objects.filter(id= id_q).values_list('peso',flat= True)
                        if rc[0] == ra:
                            pontuacao = peso_qa
                        else:
                            pontuacao = [0]
                        Respostas.objects.filter(avaliacao_has_questoes= request.POST['questao_avaliacao'], aluno= Alunos.objects.get(id= request.user.id)).update(resposta_correta= request.POST['resposta'], pontuacao= pontuacao[0])
                if 'resposta_verdadeiro_falso' in request.POST:
                    id_q = request.POST['questao_avaliacao']
                    qa = Avaliacao_has_questoes.objects.filter(id= id_q).values_list('questao',flat= True)
                    if 'resposta' in request.POST:
                        ra = request.POST['resposta']
                        rc = Questoes.objects.filter(id= qa).values_list('resposta_correta', flat= True)
                        peso_qa = Avaliacao_has_questoes.objects.filter(id= id_q).values_list('peso',flat= True)
                        if rc[0] == ra:
                            pontuacao = peso_qa
                        else:
                            pontuacao = [0]
                        Respostas.objects.filter(avaliacao_has_questoes= request.POST['questao_avaliacao'], aluno= Alunos.objects.get(id= request.user.id)).update(resposta_correta= request.POST['resposta'], pontuacao= pontuacao[0])

                if 'enviar' in request.POST:
                    context_dict['confirmacao'] = "Você tem certeza que quer enviar a avaliação?"
                if 'enviar_confirmacao' in request.POST:
                    aluno = Alunos.objects.get(id= request.user.id)
                    avaliacao = Avaliacao.objects.get(id= request.POST['avaliacao'])
                    avaliacao_has_questoes_questao = Avaliacao_has_questoes.objects.filter(avaliacao= avaliacao).values_list('questao', flat= True)
                    avaliacao_has_questoes_id = Avaliacao_has_questoes.objects.filter(avaliacao= avaliacao).values_list('id', flat= True)
                    avaliacao_has_questoes_peso = Avaliacao_has_questoes.objects.filter(avaliacao= avaliacao).values_list('peso', flat= True)
                    soma = 0
                    for v in range(0,avaliacao_has_questoes_questao.count()):
                        respostas = Questoes.objects.filter(id= avaliacao_has_questoes_questao[v]).values_list('resposta_correta',flat= True)
                        resposta_aluno = Respostas.objects.filter(avaliacao_has_questoes= avaliacao_has_questoes_id[v], aluno= aluno.id).values_list('resposta_correta', flat= True)
                        if resposta_aluno[0] == respostas[0]:
                            Respostas.objects.filter(avaliacao_has_questoes= avaliacao_has_questoes_id[v], aluno= aluno.id).update(pontuacao= avaliacao_has_questoes_peso[v])
                            soma = soma + avaliacao_has_questoes_peso[v]
                    try:
                        nota_final = 10/(sum(avaliacao_has_questoes_peso)/soma)
                    except:
                        nota_final = 0
                    print nota_final
                    context_dict['nota'] = round(nota_final,2)
                    Aluno_has_avaliacao.objects.get_or_create(aluno= aluno, data_hora= datetime.datetime.now(), avaliacao= avaliacao, nota_final= round(nota_final,2))
                    template = 'sap/notas.html'
                    return render(request,template,context_dict)
            else:
                return HttpResponseRedirect('/sap/index_aluno.html')


    return render(request, 'sap/avaliacao.html', context_dict)
