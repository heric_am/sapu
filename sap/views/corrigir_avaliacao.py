#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def corrigir_avaliacao(request):
    context_dict = {}
    if request.method != "POST":
        avaliacao_respondida = Aluno_has_avaliacao.objects.values('avaliacao')
        avaliacao = Avaliacao.objects.filter(professor= request.user.id, id__in= avaliacao_respondida).order_by('-data_hora_fim')
        if avaliacao:
            context_dict['avaliacao'] = avaliacao
        else:
            context_dict['falha'] = 'Não existem avaliações.'
    else:
        if 'avaliacaoe' in request.POST:
            avaliacao = request.POST['avaliacaoe']
            existe = Aluno_has_avaliacao.objects.filter(avaliacao= avaliacao).exists()
            if existe == False:
                context_dict['falha'] = "Essa avaliação ainda não foi respondida"
            else:
                context_dict['avaliacao_escolhida'] = avaliacao
    template = 'sap/corrigir_avaliacao.html'
    return render(request,template,context_dict)
