#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def relatorio_professor(request):
    context_dict ={}
    if request.method != "POST":
        avaliacao_respondida = Aluno_has_avaliacao.objects.values('avaliacao')
        avaliacao = Avaliacao.objects.filter(professor= request.user.id, id__in= avaliacao_respondida).order_by('-data_hora_fim')
        if avaliacao:
            context_dict['avaliacao'] = avaliacao
        else:
            context_dict['falha1'] = "Não existem avaliações"
    else:
        avaliacao = request.POST['avaliacao']
        if 'aluno' in request.POST:
            aluno = request.POST['aluno']
            print aluno
            avaliacao = request.POST['avaliacao']
            avaliacao_questao = Avaliacao_has_questoes.objects.filter(avaliacao= avaliacao).order_by('ordem')
            respostas_aluno = Respostas.objects.filter(avaliacao_has_questoes= avaliacao_questao, aluno= aluno).values_list('resposta_correta', flat= True).order_by('avaliacao_has_questoes__ordem')
            respostas_aluno_id = Respostas.objects.filter(avaliacao_has_questoes= avaliacao_questao, aluno= aluno).values_list('id', flat= True).order_by('avaliacao_has_questoes__ordem')
            respostas_aluno_pontuacao = Respostas.objects.filter(avaliacao_has_questoes= avaliacao_questao, aluno= aluno).values_list('pontuacao', flat= True).order_by('avaliacao_has_questoes__ordem')
            context_dict['avaliacao_questao'] = avaliacao_questao
            context_dict['respostas_aluno'] = respostas_aluno
            context_dict['respostas_aluno_id'] = respostas_aluno_id
            context_dict['respostas_aluno_pontuacao'] = respostas_aluno_pontuacao
            context_dict['aluno'] = Alunos.objects.get(id= aluno)
            context_dict['avaliacao'] = Avaliacao.objects.get(id= avaliacao)
        else:
            notas = Aluno_has_avaliacao.objects.filter(avaliacao= avaliacao)
            if notas:
                context_dict['notas'] = notas
                context_dict['avaliacao'] = Avaliacao.objects.get(id= avaliacao)
            else:
                context_dict['falha'] = "Esta avaliação ainda não foi respondida por nenhum aluno."

    template = 'sap/relatorio_professor.html'
    return render(request,template,context_dict)