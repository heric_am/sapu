#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def editar_avaliacao(request):
    context_dict ={}
    if request.method != "POST":
        avaliacao_respondida = Aluno_has_avaliacao.objects.values('avaliacao')
        avaliacao = Avaliacao.objects.filter(professor= request.user.id).order_by('-data_hora_fim').exclude(id__in= avaliacao_respondida)
        if avaliacao:
            context_dict['avaliacao'] = avaliacao
        else:
            context_dict['falha'] = 'Não existem avaliações.'
    else:
        if 'avaliacaoe' in request.POST:
            avaliacao = request.POST['avaliacaoe']
            existe = Aluno_has_avaliacao.objects.filter(avaliacao= avaliacao).exists()
            a = Avaliacao.objects.get(id= avaliacao)
            if existe == True or a.data_hora_inicio < datetime.datetime.now() and a.data_hora_fim > datetime.datetime.now():
                context_dict['falha'] = "Você não pode alterar essa avaliação, pois ela ja foi ou esta sendo respondida. Se deseja reutiliza-la, escolha a opção Reaplicar Avaliação e edite-a em seguida. "
            else:
                context_dict['avaliacao_escolhida'] = avaliacao
    template = 'sap/editar_avaliacao.html'
    return render(request,template,context_dict)
