#coding: utf-8
from django.shortcuts import render, redirect
#admin

from index import *
from admin import *
from register_aluno import *
from register_professor import *
from adicionar_diciplina import *
from adicionar_disciplina_aluno import *
#checar login, logout
from user_login import *
from user_logout import *
from check_professor import *
from check_aluno import *
from index_professor import *
from index_aluno import *
#professor
from criar_dissertativa import *
from excluir_questao import *
from criar_multipla_escolha import *
from criar_verdadeiro_falso import *

from criar_avaliacao import *
from vizualizar_avaliacao import *
from visualizar_gabarito import *
from editar_questao import *
from adicionar_remover_pergunta import *
from reaplicar_avaliacao import *
from editar_avaliacao import *
from alterar_dados_avaliacao import *
from excluir_avaliacao import *
from corrigir_avaliacao import *
from correcao_questao import *
from correcao_aluno import *
from relatorio_professor import *
from sair_safeexam import *
from teste import *
from serverTime import *

#alunos

from realizar_avaliacao import *
from avaliacao import *
from notas import *

