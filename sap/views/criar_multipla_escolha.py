#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from PIL import Image
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def criar_multipla_escolha(request):
    context_dict = {}
    if request.method != "POST":
        questaoform = multipla_escolhaForm()
    else:
        questaoform = multipla_escolhaForm(data=request.POST)
        if questaoform.is_valid():
            qd = questaoform.save(commit=False)
            qd.professor = Professores.objects.get(id=request.user.id)
            Tipo.objects.get_or_create(tipo='Multipla Escolha')
            x = Tipo.objects.get(tipo='Multipla Escolha')
            qd.tipo = Tipo.objects.get(id= x.id)
            r = request.POST['resposta_correta']
            qd.resposta_correta = r
            if 'imagem' in request.FILES:
                try:
                    imagem = request.FILES['imagem']
                    im= Image.open(imagem)
                    qd.imagem = imagem
                except:
                    context_dict['falha'] = 'Formato de imagem não suportado.'
                    template ='sap/criar_multipla_escolha.html'
                    return render(request, template , context_dict)
            qd.save()
            questaoform.save_m2m()
            context_dict['ultima'] = Questoes.objects.filter(professor= request.user).last()
    context_dict['questaoform'] = questaoform
    template ='sap/criar_multipla_escolha.html'
    return render(request, template , context_dict)
