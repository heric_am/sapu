#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from PIL import Image
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def criar_avaliacao(request):
    context_dict = {}
    if request.method != "POST":
        context_dict['disciplinaescolhida'] = Disciplina.objects.all().order_by('nome')
        avaliacaoform = avaliacaoForm()
    else:
        avaliacaoform = avaliacaoForm()
        context_dict['avaliacaoform'] = avaliacaoform
        if 'disciplinaescolhida' in request.POST:
            questoes_disciplina_professor = Questoes.objects.filter(disciplina= request.POST['disciplinaescolhida']).filter(professor= request.user)
            if questoes_disciplina_professor:
                context_dict['questoes_disciplina_professor'] = questoes_disciplina_professor
                context_dict['disciplina'] = request.POST['disciplinaescolhida']
            else:
                context_dict['falha'] = 'Não existem questões vinculadas a está disciplina.'
            template ='sap/criar_avaliacao.html'
            return render(request, template , context_dict)

        if 'nome' in request.POST:
            avaliacaoform = avaliacaoForm(data= request.POST)
            if avaliacaoform.is_valid():
                ca = avaliacaoform.save(commit=False)
                if ca.data_hora_inicio < datetime.datetime.now():
                    context_dict['data_invalida'] = "Você não pode escolher uma data anterior a atual."
                    template ='sap/criar_avaliacao.html'
                    return render(request, template , context_dict)
                if ca.data_hora_inicio < ca.data_hora_fim:
                    ca.professor = Professores.objects.get(id= request.user.id)
                    ca.disciplina = Disciplina.objects.get(id= request.POST['disciplina'])
                    if 'imagem' in request.FILES:
                        try:
                            print request.FILES['imagem']
                            imagem = request.FILES['imagem']
                            im= Image.open(imagem)
                            ca.imagem = imagem
                        except:
                            context_dict['falha'] = 'Formato de imagem não suportado.'
                            template ='sap/criar_avaliacao.html'
                            return render(request,template,context_dict)
                    ca.save()
                    questao = request.POST.getlist('questao')
                    peso = filter(None,request.POST.getlist('peso'))
                    ordem = filter(None,request.POST.getlist('ordem'))
                    #Criar função \/ ordenação de listas com numeros repetidos
                    conv = []
                    for item in ordem:
                        conv.append(int(item))
                    ordem = conv
                    na= len(ordem)
                    nova_lista = list(set(ordem))
                    nnova_list = len(nova_lista)
                    if na != nnova_list:
                        for v in range (len(nova_lista), (na)):
                            nova_lista.append(int(max(nova_lista)) + 1)
                        ordem = nova_lista
                    #Criar função /\
                    if questao and peso and ordem:
                        for x in range(0,len(questao)):
                            Avaliacao_has_questoes.objects.get_or_create\
                                  (avaliacao= Avaliacao.objects.latest('id'), questao= Questoes.objects.get(id= questao[x]), peso= peso[x], ordem= ordem[x])
                        context_dict['sucess'] = 'Avaliação criada com sucesso.'
                    else:
                        context_dict['falha'] = 'Você deve selecionar ao menos uma questão.'
                else:
                    context_dict['data_invalida'] = "A data invalida"
            context_dict['questoes_disciplina_professor'] = Questoes.objects.filter(disciplina= request.POST['disciplina']).filter(professor= request.user)
            context_dict['disciplina'] = request.POST['disciplina']
    context_dict['avaliacaoform'] = avaliacaoform
    template ='sap/criar_avaliacao.html'
    return render(request, template , context_dict)
