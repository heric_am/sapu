#coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_aluno, login_url='login.html')
def notas(request):
    context_dict ={}
    if request.method !=  "POST":
        aluno = Alunos.objects.get(id= request.user.id)
        avaliacao = Aluno_has_avaliacao.objects.filter(aluno= aluno)
        context_dict['avaliacao'] = avaliacao
        template = 'sap/notas.html'
        return render(request,template,context_dict)
    else:
        if 'aluno' in request.POST:
            avaliacao = request.POST['avaliacao']
            aluno = request.POST['aluno']
            Mostrar_Respostas = Avaliacao.objects.filter(id= avaliacao).values_list('Mostrar_Respostas', flat= True)
            if Mostrar_Respostas[0] == False:
                context_dict['restrito'] = 'Você não pode ver as respostas desta avaliação.'
            else:
                avaliacao_questao = Avaliacao_has_questoes.objects.filter(avaliacao= avaliacao).order_by('ordem')
                respostas_aluno = Respostas.objects.filter(avaliacao_has_questoes= avaliacao_questao, aluno= aluno).values_list('resposta_correta', flat= True).order_by('avaliacao_has_questoes__ordem')
                respostas_aluno_id = Respostas.objects.filter(avaliacao_has_questoes= avaliacao_questao, aluno= aluno).values_list('id', flat= True).order_by('avaliacao_has_questoes__ordem')
                respostas_aluno_pontuacao = Respostas.objects.filter(avaliacao_has_questoes= avaliacao_questao, aluno= aluno).values_list('pontuacao', flat= True).order_by('avaliacao_has_questoes__ordem')
                nota_final = Aluno_has_avaliacao.objects.filter(aluno= aluno, avaliacao= avaliacao).values_list('nota_final', flat= True)
                context_dict['avaliacao_questao'] = avaliacao_questao
                context_dict['respostas_aluno'] = respostas_aluno
                context_dict['respostas_aluno_id'] = respostas_aluno_id
                context_dict['respostas_aluno_pontuacao'] = respostas_aluno_pontuacao
                context_dict['aluno'] = Alunos.objects.get(id= aluno)
                context_dict['avaliacao'] = Avaliacao.objects.get(id= avaliacao)
                context_dict['nota_final'] = nota_final[0]

        template = 'sap/notas.html'
        return render(request,template,context_dict)

