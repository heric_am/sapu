    #coding: utf-8

import user
from django.http import HttpResponse
from django.shortcuts import render, redirect
from sap.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from sap.forms import *
from check_professor import *
from check_aluno import *
from django.db.models import Sum
import datetime
from django_user_agents.utils import *
reload(sys)
sys.setdefaultencoding('utf-8')
@login_required()
@user_passes_test(check_professor, login_url='login.html')
def correcao_questao(request, avaliacao):
    context_dict={}
    if request.method != 'POST':
            context_dict['avaliacao'] = avaliacao
            questoes = Questoes.objects.filter(avaliacao= avaliacao, tipo= 1, professor= request.user.id)
            if questoes:
                context_dict['questoes'] = questoes
            else:
                context_dict['falha'] = 'Está avaliação não possui questões dissertativas.'
    else:
        if 'resposta_id' in request.POST:
            resposta_id = request.POST.getlist('resposta_id')
            pontuacao = request.POST.getlist('pontuacao')
            aluno = request.POST.getlist('aluno_id')
            for x in range (0,int(len(resposta_id))):
                if pontuacao[x] != '':
                    Respostas.objects.filter(id= resposta_id[x]).update(pontuacao= pontuacao[x])
                else:
                    Respostas.objects.filter(id= resposta_id[x]).update(pontuacao= 0)
            for x in range (0,int(len(aluno))):
                av_qu = Avaliacao_has_questoes.objects.filter(avaliacao= avaliacao).values_list('id', flat= True)
                pontos = Respostas.objects.filter(avaliacao_has_questoes= av_qu, aluno= aluno[x]).aggregate(Sum('pontuacao'))
                pontos = pontos['pontuacao__sum']
                avaliacao_questao_peso = Avaliacao_has_questoes.objects.filter(avaliacao= avaliacao).values_list('peso', flat= True)
                nota_final = 10/(sum(avaliacao_questao_peso)/pontos)
                Aluno_has_avaliacao.objects.filter(avaliacao= avaliacao, aluno= aluno[x]).update(nota_final= round(nota_final,2))

            context_dict['avaliacao'] = avaliacao
            questoes = Questoes.objects.filter(avaliacao= avaliacao, tipo= 1, professor= request.user.id)
            if questoes:
                context_dict['questoes'] = questoes
            #context_dict['sucess'] = "Questões corrigidas com sucesso."
        else:
            try:
                avaliacao_questao = Avaliacao_has_questoes.objects.get(avaliacao= avaliacao, questao= request.POST['questao'])
                respostas = Respostas.objects.filter(avaliacao_has_questoes= avaliacao_questao)
                context_dict['avaliacao'] = avaliacao
                context_dict['avaliacao_questao'] = avaliacao_questao
                context_dict['respostas_aluno'] = respostas
            except:
                context_dict['falha'] = 'Você deve selecionar ao menos uma questão.'

    template = 'sap/correcao_questao.html'
    return render(request,template,context_dict)