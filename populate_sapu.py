#!/user/bin/env python
#coding: utf-8
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sapu.settings')

import django
django.setup()

from sap.models import *

def populate():
    eduardo_silva = register_professor(username= 'Eduardo', password= 'Estrutur@ded@dos')
    p1 = register_professor(username= 'p1', password= 'a')
    joice_mota = register_professor(username= 'Joice', password= 'Desenv0lviment0')
    ivo_riegel = register_professor(username= 'Ivo', password= 'B@ncodeD@dos')

    a1= register_aluno(username='felipe_flamarion', password= 'programacao1')
    a2 = register_aluno(username= 'kasten_correa', password= 'estruturadedados')
    a3 = register_aluno(username= 'patrick_felipe', password= 'bancodedados1')
    a4 = register_aluno(username= 'vanessa_barbosa', password= 'desenvolvimentoweb1')
    a5 = register_aluno(username= 'andreson_pontes', password= 'etica')
    a6 = register_aluno(username= 'antonio_castano', password= 'legislacao')
    a7 = register_aluno(username= 'diego_rodrigues', password= 'bancodedados2')
    a8 = register_aluno(username= 'iago_marinheiro', password= 'redes2')
    a9 = register_aluno(username= 'jessica_schneider', password= 'redessemfio')
    a10 = register_aluno(username= 'maricleia_vieira', password= 'tcc1')
    a11 = register_aluno(username= 'marilia_ribeiro', password= 'tcc2')
    a12 = register_aluno(username= 'nicole_oliveira', password= 'analisedesistemas')
    a13 = register_aluno(username= 'rafael_carneiro', password= 'matematicaaplicada')
    a14 = register_aluno(username= 'rodrigo_moreira', password= 'gestaodeti')
    a15 = register_aluno(username= 'wagner_esser', password= 'poo1')
    a16 = register_aluno(username= 'wellington_willian', password= 'poo2')
    a17 = register_aluno(username= 'giorgy_ismael', password= 'gsi1')
    a18= register_aluno(username='a1', password= 'a')

    Redes_sem_fio = register_diciplina(nome= 'Redes sem Fio')
    Baco_de_dados = register_diciplina(nome= 'Banco de Dados')

    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a1)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a2)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a3)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a4)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a5)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a6)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a7)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a8)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a9)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a10)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a11)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a12)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a13)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a14)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a15)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a16)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a17)
    register_disciplina_has_aluno(disciplina= Redes_sem_fio, aluno= a18)

    t1 = register_tipo(tipo= "Dissertativa")
    t2 = register_tipo(tipo= "Multipla Escolha")
    t3 = register_tipo(tipo= "Verdadeiro ou Falso")

def register_professor(username, password):
    r = Professores.objects.get_or_create(username= username, password= password)[0]
    r.set_password(password)
    r.save()
    last = Professores.objects.latest('id')
    Group.objects.get_or_create(name='Professores')
    add_grupo = Group.objects.get(name='Professores')
    add_grupo.user_set.add(last)
    return r
def register_aluno(username, password):
    r = Alunos.objects.get_or_create(username= username, password= password)[0]
    r.set_password(password)
    r.save()
    last = Alunos.objects.latest('id')
    Group.objects.get_or_create(name='Alunos')
    add_grupo = Group.objects.get(name='Alunos')
    add_grupo.user_set.add(last)
    return r
def register_diciplina(nome):
    r = Disciplina.objects.get_or_create(nome= nome)[0]
    r.save()
    return r
def register_disciplina_has_aluno(disciplina, aluno):
    r = Disciplina_has_aluno.objects.get_or_create(disciplina= disciplina, aluno= aluno)[0]
    r.save()
    return r
def register_tipo(tipo):
    r = Tipo.objects.get_or_create(tipo= tipo)[0]
    r.save()
    return r

# Start execution here!
if __name__ == '__main__':
    print "Starting Rango population script..."
    populate()
