function serverTime() {
    var time;
    $.ajax({url: '/sap/serverTime/',
        async: false, dataType: 'text',
        success: function(text) {
            time = new Date(text);
        }, error: function(http, message, exc) {
            time = new Date();
    }});
    return time;
}